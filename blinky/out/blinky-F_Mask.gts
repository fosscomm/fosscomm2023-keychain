G04 #@! TF.GenerationSoftware,KiCad,Pcbnew,7.0.1*
G04 #@! TF.CreationDate,2023-08-24T21:02:16+03:00*
G04 #@! TF.ProjectId,blinky,626c696e-6b79-42e6-9b69-6361645f7063,rev?*
G04 #@! TF.SameCoordinates,Original*
G04 #@! TF.FileFunction,Soldermask,Top*
G04 #@! TF.FilePolarity,Negative*
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW 7.0.1) date 2023-08-24 21:02:16*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
G04 Aperture macros list*
%AMRoundRect*
0 Rectangle with rounded corners*
0 $1 Rounding radius*
0 $2 $3 $4 $5 $6 $7 $8 $9 X,Y pos of 4 corners*
0 Add a 4 corners polygon primitive as box body*
4,1,4,$2,$3,$4,$5,$6,$7,$8,$9,$2,$3,0*
0 Add four circle primitives for the rounded corners*
1,1,$1+$1,$2,$3*
1,1,$1+$1,$4,$5*
1,1,$1+$1,$6,$7*
1,1,$1+$1,$8,$9*
0 Add four rect primitives between the rounded corners*
20,1,$1+$1,$2,$3,$4,$5,0*
20,1,$1+$1,$4,$5,$6,$7,0*
20,1,$1+$1,$6,$7,$8,$9,0*
20,1,$1+$1,$8,$9,$2,$3,0*%
G04 Aperture macros list end*
%ADD10RoundRect,0.243750X0.456250X-0.243750X0.456250X0.243750X-0.456250X0.243750X-0.456250X-0.243750X0*%
G04 APERTURE END LIST*
D10*
X116332000Y-79880700D03*
X116332000Y-78005700D03*
X143789400Y-79550500D03*
X143789400Y-77675500D03*
M02*
