# fosscomm2023-keychain
The official keychain for the [fosscomm 2023](https://2023.fosscomm.gr/) event. Designed by [TOLABAKI](https://wiki.tolabaki.gr/w/To_LABaki) hackerspace, using the [KiCad](https://www.kicad.org/) open source software suite.

![](imgs/front.png)
![](imgs/back.png)